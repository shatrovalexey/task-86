@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
			@if ( ( $user = auth( )->user( ) ) && $user->is_admin )
			<a href="/admin">админка</a>
			@endif
            <div class="card">
				@if ( $errors )
				<h3>Ошибки</h3>
				<ul>
					@foreach ( $errors as $error )
					<li>
						{{$error}}
					</li>
					@endforeach
				</ul>
				@endif
                <div class="card-header">Поиск</div>
                <div class="card-body">
					<form>
						<div>укажите ингридиенты</div>
						<select multiple name="node_list[]" size="5" style="width: 100% ;" required>
						@foreach ( $nodes as $node )
							<option value="{{$node->id}}"
							@if ( in_array( $node->id , $node_list ) )
							selected
							@endif
							>{{$node->title}}</option>
						@endforeach
						</select>

						<label>
							<span>найти</span>
							<input type="submit" value="&rarr;">
						</label>
					</form>

					@if ( count( $receipts ) > 0 )
					<h2>Рецепты</h2>
					<table class="table">
						<thead>
							<tr>
								<td>#</td>
								<td>название</td>
								<td>совпадения</td>
								<td>ингридиенты</td>
							</tr>
						</thead>
						@foreach ( $receipts as $receipt )
						<tr>
							<td>
								<span>{{$receipt->id}}</span>
							</td>
							<td>
								<span>{{$receipt->title}}</span>
							</td>
							<td>
								<span>{{$receipt->weight}}</span>
							</td>
							<td>
								<span>{{$receipt->node_list}}</span>
							</td>
						</tr>
						@endforeach
					</table>

					{{ $receipts->links() }}
					@endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection