@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Рецепт</div>
                <div class="card-body">
					<p><a href="/admin/receipt">список всех рецептов</a>
					<form method="post" action="/admin/receipt/{{$receipt->id}}/update">
						{{ csrf_field() }}
						<label>
							<div>название</div>
							<input name="title" value="{{$receipt->title}}" style="width: 100% ;" required>
						</label>

						<label>
							<span>сохранить</span>
							<input type="submit" value="&rarr;">
						</label>
					</form>

					<form method="post" action="/admin/receipt/{{$receipt->id}}/nodes">
						{{ csrf_field() }}
						<fieldset>
							<legend>
								<h3>ингридиенты</h3>
							</legend>

							<div>
								<p>Чтобы скрыть или показать ингридиент нажмите на ссылку возле его названия.
								<p>Чтобы добавить или удалить ингридиент нужно только указать или убрать галочку. Кнопки "сохранить" нет.
							</div>

							@foreach ( $receipt->nodes( )->get( ) as $node )
							<label data-is_hidden="{{intval($node->is_hidden)}}">
								<input type="checkbox" name="node_list[]" value="{{$node->id}}" class="node_list-item"
									data-node_id="{{$node->id}}"
								@if ( $node->selected > 0 )
									checked
								@endif
								>
								<span>{{$node->title}}</span>
								@if ( $node->selected > 0 )
								<span>
									<a href="/admin/receipt/{{$receipt->id}}/{{$node->id}}/{{intval(! $node->is_hidden)}}/toggle">
										@if ( $node->is_hidden > 0 )
											<span title="показать">V</span>
										@else
											<span title="скрыть">X</span>
										@endif
									</a>
								</span>
								@endif
							</label>
							@endforeach
						</fieldset>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
jQuery( function( ) {
	jQuery( ".node_list-item" ).on( "change" , function( ) {
		let $self = jQuery( this ) ;
		let $form = $self.parents( "form:first" ) ;
		let $node_id = $self.data( "node_id" ) ;
		let $csrf = jQuery( "meta[name='csrf-token']" ).attr( "content" ) ;

		jQuery.ajax( {
			"url" : $form.attr( "action" ) ,
			"type" : $form.attr( "method" ) ,
			"data" : {
				"node_id" : $node_id ,
				"toggle" : $self.prop( "checked" ) ? 1 : 0 ,
				"_token" : $csrf ,
			} ,
			"dataType" : "json" ,
			"error" : function( ) {
				alert( "Внутренняя ошибка" ) ;
			}
		} ) ;
	} ) ;
} ) ;
</script>
<style>
*[name="node_list[]"] + span {
	font-family: Tahoma ;
}
*[name="node_list[]"]:checked + span {
	font-weight: bold ;
}
*[data-is_hidden] {
	display: inline-block ;
	width: 24% ;
	border: 1px silver solid ;
	margin: 2px ;
	padding: 2px ;
}
*[data-is_hidden="1"] {
	font-style: italic ;
	font-weight: normal ;
}
</style>
@endsection