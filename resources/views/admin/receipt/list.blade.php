@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
				<p><a href="/admin">админка</a>

                <div class="card-header">Рецепты</div>

				<form method="post" action="/admin/receipt/create">
					{{ csrf_field() }}
					<fieldset>
						<label>
							<span>название</span>
							<input name="title">
						</label>
						<label>
							<span>создать</span>
							<input type="submit" value="&rarr;">
						</label>
					</fieldset>
				</form>

                <div class="card-body">
					<table class="table">
						<caption>
							<h2>Рецепты</h2>
						</caption>
						<thead>
							<tr>
								<td>#</td>
								<td>название</td>
								<td>удалить</td>
								<td>редактировать</td>
							</tr>
						</thead>
						@foreach ( $receipts as $receipt )
						<tr>
							<td>
								<span>{{$receipt->id}}</span>
							</td>
							<td>
								<span>{{$receipt->title}}</span>
							</td>
							<td>
								<a href="/admin/receipt/{{$receipt->id}}/del">X</a>
							</td>
							<td>
								<a href="/admin/receipt/{{$receipt->id}}/edit">&rarr;</a>
							</td>
						</tr>
						@endforeach
					</table>

					{{ $receipts->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
