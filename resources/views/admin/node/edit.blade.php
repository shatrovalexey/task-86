@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ингридиент</div>
                <div class="card-body">
					<p><a href="/admin/node">список всех ингридиентов</a>
					<form method="post" action="/admin/node/{{$node->id}}/update">
						{{ csrf_field() }}
						<label>
							<div>название</div>
							<input name="title" value="{{$node->title}}" style="width: 100% ;" required>
						</label>

						<label>
							<span>сохранить</span>
							<input type="submit" value="&rarr;">
						</label>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>