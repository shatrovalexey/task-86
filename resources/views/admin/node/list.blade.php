@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
				<p><a href="/admin">админка</a>

                <div class="card-header">Ингридиенты</div>

				<form method="post" action="/admin/node/create">
					{{ csrf_field() }}
					<fieldset>
						<label>
							<span>название</span>
							<input name="title">
						</label>
						<label>
							<span>создать</span>
							<input type="submit" value="&rarr;">
						</label>
					</fieldset>
				</form>

                <div class="card-body">
					<table class="table">
						<caption>
							<h2>Ингридиенты</h2>
						</caption>
						<thead>
							<tr>
								<td>#</td>
								<td>название</td>
								<td>удалить</td>
								<td>редактировать</td>
							</tr>
						</thead>
						@foreach ( $nodes as $node )
						<tr>
							<td>
								<span>{{$node->id}}</span>
							</td>
							<td>
								<span>{{$node->title}}</span>
							</td>
							<td>
								<a href="/admin/node/{{$node->id}}/del">X</a>
							</td>
							<td>
								<a href="/admin/node/{{$node->id}}/edit">&rarr;</a>
							</td>
						</tr>
						@endforeach
					</table>

					{{ $nodes->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection