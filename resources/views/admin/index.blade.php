@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Админка</div>
					<ul>
						<li>
							<a href="/admin/receipt">Рецепты</a>
						</li>
						<li>
							<a href="/admin/node">Ингридиенты</a>
						</li>
					</ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection