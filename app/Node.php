<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
* Ингридиент
*/
class Node extends Model {
	/**
	* @var string $table - имя таблицы в БД
	*/
	protected $table = 'node' ;
}