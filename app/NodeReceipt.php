<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
* Связь "ингридиент" <<->> "рецепт"
*/
class NodeReceipt extends Model {
	/**
	* @var string $table - имя таблицы в БД
	*/
	protected $table = 'node_receipt' ;

	/**
	* Создать
	*
	* @param integer $receipt_id - идентификатор рецепта
	* @param integer $node_id - идентификатор ингридиента
	*/
	public static function add( int $receipt_id , int $node_id ) {
		return static::insertOrIgnore( [
			'receipt_id' => $receipt_id ,
			'node_id' => $node_id ,
		] ) ;
	}

	/**
	* Удалить
	*
	* @param integer $receipt_id - идентификатор рецепта
	* @param integer $node_id - идентификатор ингридиента
	*/
	public static function del( $receipt_id , $node_id ) {
		return static::where( [
			'receipt_id' => $receipt_id ,
			'node_id' => $node_id ,
		] )->delete( ) ;
	}

	/**
	* Скрыть\показать
	*
	* @param integer $receipt_id - идентификатор рецепта
	* @param integer $node_id - идентификатор ингридиента
	* @param boolean $is_hidden - связь скрыта
	*/
	public static function toggle( int $receipt_id , int $node_id , bool $is_hidden = false ) {
		return static::where( [
			'receipt_id' => $receipt_id ,
			'node_id' => $node_id ,
		] )->update( [
			'is_hidden' => $is_hidden ,
		] ) ;
	}
}