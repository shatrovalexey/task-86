<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB ;
use App\Node ;
use App\NodeReceipt ;

/**
* Рецепт
*/
class Receipt extends Model {
	/**
	* @var string $table - имя таблицы в БД
	*/
	protected $table = 'receipt' ;

	/**
	* Добавить ингридиент
	* @param App\Node $node - ингридиент
	*/
	public function addNode( Node $node ) {
		return NodeReceipt::add( $this , $node ) ;
	}

	/**
	* Удалить ингридиент
	* @param App\Node $node - ингридиент
	*/
	public function delNode( Node $node ) {
		return NodeReceipt::del( $this , $node ) ;
	}

	/**
	* Скрыть\показать ингридиент
	* @param integer $node_id - идентификатор ингридиента
	*/
	public function toggleNode( int $node_id ) {
		return NodeReceipt::toggle( $this->id , $node_id ) ;
	}

	/**
	* Список ингридиентов
	*/
	public function nodes( ) {
		$receipt_id = $this->id ;

		return Node::leftJoin( 'node_receipt' , function( $join ) use( &$receipt_id ) {
			$join->on( 'node.id' , '=' , 'node_receipt.node_id' ) ;
			$join->where( [
				'node_receipt.receipt_id' => $receipt_id ,
			] ) ;
		} )->select( 'node.id' , 'node.title' , 'node_receipt.node_id AS selected' , 'node_receipt.is_hidden' )
		->orderBy( 'node.title' , 'asc' ) ;
	}

	/**
	* Поиск по списку ингридиентов
	* @param array of integer $node_list - список идентификаторов ингридиентов
	*/
	public static function search( array $node_list ) {
		return DB::table( 'receipt' )
			->selectRaw( "
				receipt.id ,
				receipt.title ,
				count( DISTINCT node_receipt.node_id ) AS `weight` ,
				group_concat( DISTINCT node.title ORDER BY 1 ASC SEPARATOR ', ' ) AS `node_list`
			" )
			->leftJoin( 'node_receipt AS nr1' , function( $join ) {
				$join->on( 'nr1.receipt_id' , '=' , 'receipt.id' )
					->where( 'nr1.is_hidden' , '=' , '0' ) ;
			} )
			->leftJoin( 'node' , 'nr1.node_id' , '=' , 'node.id' )
			->leftJoin( 'node_receipt' , 'node_receipt.receipt_id' , '=' , 'receipt.id' )
			->whereIn( 'node_receipt.node_id' , $node_list )
			->where( 'node_receipt.is_hidden' , '=' , '0' )
			->groupByRaw( '1 , 2' )
			->havingRaw( 'count( DISTINCT node_receipt.node_id ) >= 2' )
			->orderByRaw( '3 DESC' ) ;
	}
}