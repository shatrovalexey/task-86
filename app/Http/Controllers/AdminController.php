<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB ;
use App\Receipt ;
use App\NodeReceipt ;
use App\Node ;

class AdminController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( ) {
		$this->middleware( 'auth' ) ;
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
	public function index( ) {
		return view( 'admin.index' ) ;
	}
}