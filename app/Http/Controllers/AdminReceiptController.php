<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB ;
use Illuminate\Support\Facades\Validator;
use App\Receipt ;
use App\NodeReceipt ;
use App\Node ;

/**
* Рецепт
*/
class AdminReceiptController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( ) {
		$this->middleware( 'auth' ) ;
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
	public function list( ) {
		return view( 'admin.receipt.list' , [
			'receipts' => Receipt::paginate( 10 ) ,
		] ) ;
	}

	/**
	* Добавление\удаление ингридиента
	*
	* @param Illuminate\Http\Request $request - запрос
	* @param integer $receipt_id - идентификатор рецепта
	*/
	public function nodes( Request $request , int $receipt_id ) {
		$node_id = $request->post( 'node_id' ) ;
		$toggle = $request->post( 'toggle' ) ;
		$result = null ;

		if ( empty( $toggle ) ) {
			$result = NodeReceipt::del( $receipt_id , $node_id ) ;
		} else {
			$result = NodeReceipt::add( $receipt_id , $node_id ) ;
		}

		return [ 'result' => $result , ] ;
	}

	/**
	* Обновить
	*
	* @param Illuminate\Http\Request $request - запрос
	* @param integer $receipt_id - идентификатор рецепта
	*/
	public function update( Request $request , int $receipt_id ) {
		$validator = Validator::make( $request->post( ) , [
			'title' => 'required' ,
		] ) ;

		if ( $validator->fails( ) ) {
			return redirect( )->back( )->withErrors( $validator ) ;
		}

		$receipt = Receipt::where( [ 'id' => $receipt_id , ] )->first( ) ;
		$receipt->title = $request->post( 'title' ) ;
		$receipt->save( ) ;

		return redirect( )->back( ) ;
	}

	/**
	* Создать
	*
	* @param Illuminate\Http\Request $request - запрос
	*/
	public function create( Request $request ) {
		$validator = Validator::make( $request->post( ) , [
			'title' => 'required' ,
		] ) ;

		if ( $validator->fails( ) ) {
			return redirect( )->back( )->withErrors( $validator ) ;
		}

		$receipt = new Receipt( ) ;
		$receipt->title = $request->post( 'title' ) ;
		$receipt->save( ) ;

		return redirect( )->route( 'receipt-edit' , [ 'receipt_id' => $receipt->id , ] ) ;
	}

	/**
	* Изменить
	*
	* @param integer $receipt_id - идентификатор рецепта
	*/
	public function edit( int $receipt_id ) {
		return view( 'admin.receipt.edit' , [
			'receipt' => Receipt::where( [
				'id' => $receipt_id ,
			] )->first( ) ,
		] ) ;
	}

	/**
	* Удалить
	*
	* @param integer $receipt_id - идентификатор рецепта
	*/
	public function del( int $receipt_id ) {
		Receipt::where( [ 'id' => $receipt_id , ] )->delete( ) ;

		return redirect( )->back( ) ;
	}

	/**
	* Скрыть\показать ингридиент
	*
	* @param integer $receipt_id - идентификатор рецепта
	* @param integer $node_id - идентификатор ингридиента
	* @param integer $is_hidden - скрыть или показать
	*/
	public function toggle( int $receipt_id , int $node_id , int $is_hidden = 0 ) {
		NodeReceipt::toggle( $receipt_id , $node_id , $is_hidden ) ;

		return redirect( )->back( ) ;
	}
}