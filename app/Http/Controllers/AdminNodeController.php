<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB ;
use Illuminate\Support\Facades\Validator;
use App\Receipt ;
use App\NodeReceipt ;
use App\Node ;

/**
* Ингридиенты
*/
class AdminNodeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( ) {
		$this->middleware( 'auth' ) ;
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
	public function list( ) {
		return view( 'admin.node.list' , [
			'nodes' => Node::paginate( 10 ) ,
		] ) ;
	}

	/**
	* Обновить
	*
	* @param Illuminate\Http\Request $request - запрос
	* @param integer $node_id - идентификатор ингридиента
	*/
	public function update( Request $request , int $node_id ) {
		$validator = Validator::make( $request->post( ) , [
			'title' => 'required' ,
		] ) ;

		if ( $validator->fails( ) ) {
			return redirect( )->back( )->withErrors( $validator ) ;
		}

		$receipt = Node::where( [ 'id' => $node_id , ] )->first( ) ;
		$receipt->title = $request->post( 'title' ) ;
		$receipt->save( ) ;

		return redirect( )->back( ) ;
	}

	/**
	* Создать
	*
	* @param Illuminate\Http\Request $request - запрос
	*/
	public function create( Request $request ) {
		$validator = Validator::make( $request->post( ) , [
			'title' => 'required' ,
		] ) ;

		if ( $validator->fails( ) ) {
			return redirect( )->back( )->withErrors( $validator ) ;
		}

		$node = new Node( ) ;
		$node->title = $request->post( 'title' ) ;
		$node->save( ) ;

		return redirect( )->back( ) ;
	}

	/**
	* Форма редактирования
	*
	* @param integer $node_id - идентификатор ингридиента
	*/
	public function edit( int $node_id ) {
		return view( 'admin.node.edit' , [
			'node' => Node::where( [ 'id' => $node_id , ] )->first( ) ,
		] ) ;
	}

	/**
	* Удалить
	*
	* @param integer $node_id - идентификатор ингридиента
	*/
	public function del( int $node_id ) {
		Node::where( [
			'id' => $node_id ,
		] )->delete( ) ;

		return redirect( )->back( ) ;
	}
}