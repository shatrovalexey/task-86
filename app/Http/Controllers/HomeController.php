<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request ;
use App\Node ;
use App\Receipt ;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( ) {
		$this->middleware( 'auth' ) ;
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index( Request $request ) {
		$node_list = $request->get( 'node_list' ) ;

		if ( ! empty( $node_list ) ) {
			if ( ! is_array( $node_list ) ) {
				return redirect( )->back( )->withErrors( [
					'error' => 'неправильный формат запроса' ,
				] ) ;
			}
			if ( count( $node_list ) < 2 ) {
				return redirect( )->back( )->withErrors( [
					'error' => 'не ленись, добавь веществ' ,
				] ) ;
			}
		}

		if ( empty( $node_list ) || ! is_array( $node_list ) ) {
			$node_list = [ ] ;
		}

		$errors = [ ] ;

		if ( empty( $node_list ) ) {
			$receipts = [ ] ;
		} else {
			$receipts = Receipt::search( $node_list ) ;
			$receipts = $receipts->paginate( 15 ) ;

			if ( ! $receipts->count( ) ) {
				$errors[] = 'не найдено лекарств' ;
			}
		}

        return view( 'search' , [
			'errors' => $errors ,
			'receipts' => $receipts ,
			'nodes' => Node::get( ) ,
			'node_list' => $node_list ,
		] ) ;
    }
}