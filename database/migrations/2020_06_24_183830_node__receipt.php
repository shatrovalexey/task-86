<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NodeReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
		Schema::create( 'node_receipt' , function ( Blueprint $table ) {
			$table->bigIncrements( 'id' ) ;
			$table->unsignedBigInteger( 'receipt_id' ) ;
			$table->unsignedBigInteger( 'node_id' ) ;
			$table->boolean( 'is_hidden' )->default( 0 ) ;
			$table->timestamps( ) ;

			$table->unique( [ 'receipt_id' , 'node_id' ] ) ;
			$table->unique( [ 'node_id' , 'receipt_id' , 'is_hidden' ] ) ;

			$table->foreign( 'receipt_id' )->references( 'id' )->on( 'receipt' )->onDelete( 'cascade' )->onUpdate( 'cascade' ) ;
			$table->foreign( 'node_id' )->references( 'id' )->on( 'node' )->onDelete( 'cascade' )->onUpdate( 'cascade' ) ;
		} ) ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
		Schema::drop( 'node_receipt' ) ;
    }
}