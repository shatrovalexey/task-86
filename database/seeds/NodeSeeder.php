<?php

use Illuminate\Database\Seeder;

class NodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run( ) {
		$table = DB::table( 'node' ) ;

		for ( $i = 0 ; $i < 1e2 ; $i ++ ) {
			$table->insert( [
				'title' => Str::random( rand( 5 , 10 ) ) ,
			] ) ;
		}
    }
}