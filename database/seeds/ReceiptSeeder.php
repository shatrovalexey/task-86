<?php

use Illuminate\Database\Seeder;

class ReceiptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run( ) {
		$table = DB::table( 'receipt' ) ;

		for ( $i = 0 ; $i < 1e2 ; $i ++ ) {
			$table->insert( [
				'title' => Str::random( rand( 5 , 10 ) ) ,
				'created_at' => new \DateTime( ) ,
			] ) ;
		}
    }
}