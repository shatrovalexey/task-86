<?php

use Illuminate\Database\Seeder;

class NodeReceiptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run( ) {
		$node_receipts = DB::table( 'node_receipt' ) ;
		$nodes = DB::table( 'node' )->select( [ 'id' , ] )->inRandomOrder( ) ;
		$receipts = DB::table( 'receipt' )->select( [ 'id' , ] ) ;

		foreach ( $receipts->get( ) as $receipt ) {
			for ( $i = rand( 2 , 10 ) ; $i > 0 ; $i -- ) {
				$node_receipts->insertOrIgnore( [
					'receipt_id' => $receipt->id ,
					'node_id' => $nodes->first( )->id ,
				] ) ;
			}
		}
    }
}