<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get( '/' , 'HomeController@index' )->name( 'home' ) ;
Route::get( '/admin' , 'AdminController@index' )->middleWare( 'is_admin' ) ;

Route::get( '/admin/node' , 'AdminNodeController@list' )->middleWare( 'is_admin' ) ;
Route::get( '/admin/node/{node_id}/edit' , 'AdminNodeController@edit' )->name( 'node-edit' )->middleWare( 'is_admin' ) ;
Route::post( '/admin/node/{node_id}/update' , 'AdminNodeController@update' )->middleWare( 'is_admin' ) ;
Route::get( '/admin/node/{node_id}/del' , 'AdminNodeController@del' )->middleWare( 'is_admin' ) ;

Route::get( '/admin/receipt' , 'AdminReceiptController@list' )->middleWare( 'is_admin' ) ;
Route::get( '/admin/receipt/{receipt_id}/edit' , 'AdminReceiptController@edit' )->name( 'receipt-edit' )->middleWare( 'is_admin' ) ;
Route::get( '/admin/receipt/{receipt_id}/del' , 'AdminReceiptController@del' )->middleWare( 'is_admin' ) ;
Route::post( '/admin/receipt/{receipt_id}/update' , 'AdminReceiptController@update' )->middleWare( 'is_admin' ) ;
Route::post( '/admin/receipt/{receipt_id}/nodes' , 'AdminReceiptController@nodes' )->middleWare( 'is_admin' ) ;
Route::post( '/admin/receipt/create' , 'AdminReceiptController@create' )->middleWare( 'is_admin' ) ;
Route::get( '/admin/receipt/{receipt_id}/{node_id}/{is_hidden}/toggle' , 'AdminReceiptController@toggle' )->middleWare( 'is_admin' ) ;